package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    protected WebDriver driver;

    public HomePage(WebDriver driver) {

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "input[aria-label='Find jobs by keyword']")
    private WebElement searchField;

    @FindBy(id = "jSearchSubmit")
    private WebElement searchSubmit;

    @FindBy(css = "input[value='All locations'")
    private WebElement locationField;

    @FindBy(css = ".loc_icn")
    private WebElement locationSelector;

    public HomePage addSearchPhrase(String phrase) {

        searchField.clear();
        searchField.sendKeys(phrase);

        return this;
    }

    public HomePage selectLocation(String location) {

        locationSelector.click();
        locationSelector.click();
        locationField.sendKeys(location);
        locationField.sendKeys(Keys.ENTER);

        return this;
    }

    public ResultsPage submitSearch() {

        searchSubmit.click();

        return new ResultsPage(driver);
    }
}
