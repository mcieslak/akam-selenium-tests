package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ResultsPage extends HomePage {


    public ResultsPage(WebDriver driver) {

        super(driver);
    }

    @FindBy(css = ".total_results")
    private WebElement numberOfResults;

    @FindBy(id = "job_no_results_list_hldr")
    private WebElement noResultsNotification;

    public ResultsPage assertResultsAreGreaterThanZero() {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".total_results")));

        Assert.assertTrue(Integer.parseInt(numberOfResults.getText()) > 0);
        return this;
    }

    public ResultsPage assertNoResultsAreShown() {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("job_no_results_list_hldr")));

        Assert.assertTrue(noResultsNotification.isDisplayed());
        return this;


    }
}
