import org.testng.annotations.Test;
import pages.HomePage;

public class NoOffersTest extends BaseTest {

    @Test
    public void searchForLocation() {

        new HomePage(driver)
                .addSearchPhrase("XXX")
                .submitSearch()
                .assertNoResultsAreShown();
    }
}
