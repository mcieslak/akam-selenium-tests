import org.testng.annotations.Test;
import pages.HomePage;

public class JobSearchTest extends BaseTest {

    @Test
    public void searchForLocation() {

        new HomePage(driver)
                .addSearchPhrase("Test")
                .selectLocation("Krakow, Poland")
                .submitSearch()
                .assertResultsAreGreaterThanZero();
    }
}
